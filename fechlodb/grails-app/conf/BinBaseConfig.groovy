import org.codehaus.groovy.grails.commons.ConfigurationHolder as CH

/**
 * this file contains the binbase configuration for this plugin.
 *
 */
binbase {


    key = "${CH.config.external.binbase.key}"

    server = "${CH.config.external.binbase.server}"

    /**
     * the used binbase voc db for this instance
     */
    database = "rtx5"

    /**
     * the list of experiments we want to sync or the titles of it. We will try to find the id's of them in the metadata files. You can also specify true to sync all experiments or false to skip the synchornisation all together
     */
    syncExperiments = [
            /*
            "101184",
            "392100",
            "392553",
            "554193",
            "559269"
            */

            "80324"
    ]

    /**
     * we only synchronize the metadata and nothing else
     */
    syncMetaDataOnly = false

    /**
     * where can we find our metadata
     */
    metaDataPath = "./meta"

    /**
     * fetches the metadata from the given url
     */
    metaDataUrl = "http://minix.fiehnlab.ucdavis.edu/communications/studieDataAsXML"

    /**
     * strict means we need to have the metadata file or the experiment won't be syncd
     */
    strict = false

    /**
     * title of this instance
     */
    title = "The FeChloDB"

    /**
     * the binbase header, useful to describe the exact database
     */
    logoDescription = "The Iron Chlorosis Database"

    /**
     * the welcome title on the index.gsp page
     */
    welcomeTitle = "The Iron Chlorosis Database"
    /**
     * the welcome text on the index.gsp page
     */
    welcomeText = """
<bold>Iron-dependent modifications of the flower transcriptome, proteome, metabolome and hormonal content in an Arabidopsis ferritin mutant</bold>

<p>
Iron homeostasis is an important process for flower development and for plant fertility. The role of plastids in these processes has been shown to be essential. To further document the relationships between plastid iron homeostasis and flower biology, we performed a global study (transcriptome, proteome, metabolome and hormone analysis) of Arabidopsis flowers from wild type and triple atfer1-3-4 ferritin mutant plants grown under iron sufficient or excess conditions. Although no direct overlaps from transcript to proteins and to metabolites were evidenced, some major modifications in specific functional categories were consistently observed at these three various levels of gene expression. These concerned redox reactions and oxidative stress, as well as amino acid and protein catabolism, this later point being exemplified by an almost ten fold increase in urea concentration of atfer1-3-4 flowers from plants grown under iron excess conditions. The mutant background caused alterations in Fe-heme redox proteins located in membranes and in hormone responsive proteins. Specific effects of excess Fe in the mutant included further changes in these categories, supporting that the mutant is facing a more intense Fe/redox stress than the wild type. Changes in the membrane transporters and lipid metabolism also indicate the impact of the mutation and/or excess Fe is very intense at the membrane level. In spite of the large number of genes and proteins responsive to hormones found to be regulated in this study, changes in the hormonal balance were restricted to cytokinins, especially in the mutant plants grown under Fe excess conditions.
</p>
    """

    /**
     * the msp file name
     */
    mspFileName = "The FeChloDB as MSP file"

    /**
     * is the msp file enabled
     */
    mspEnabled = true

    /**
     * pre cache bin similarities
     */
    similarityPreCaching = false

    /**
     * only synchronize bins which are found in the synchronized experiments
     */
    simplifyBins = true
}
