<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>

</head>

<body>

<g:if test="${BBOrganInstance != null}">

    <g:javascript>
        $(document).ready(function() {
            $('#selectedOrgan').button({
                icons: {
                    secondary: "ui-icon-circle-minus"
                }
            });
        });
    </g:javascript>


    <g:link controller="BBSpecies" action="show" id="${BBSpeciesInstance.id}"
            style="margin-top:10px; margin-right: 18px;float: right;text-decoration: none">
        <span id="selectedOrgan">
            ${BBOrganInstance.name}
        </span>
    </g:link>

</g:if>

<div class="article">

    <h2 class="star">
        Species Details
    </h2>

    <g:if test="${flash.message}">
        <div class="message">${flash.message}</div>
    </g:if>

    <div class="dialog">
        <table>
            <tbody>

            <tr class="prop">
                <td valign="top" class="name">Species Name:</td>

                <td valign="top" class="value">${fieldValue(bean: BBSpeciesInstance, field: "name")}</td>

            </tr>

            <g:if test="${BBOrganInstance != null}">
                <td valign="top" class="name">Organ:</td>

                <td valign="top" class="value"><g:link action="show" controller="BBOrgan" id="${BBOrganInstance.id}">${fieldValue(bean: BBOrganInstance, field: "name")}</g:link></td>

            </g:if>


            <tr class="prop">
                <td valign="top" class="name">Sample Count:</td>

                <td valign="top" class="value"><span id="speciesSampleCount"></span></td>

                <g:javascript>
  $(document).ready(function() {

        $('#speciesSampleCount').load('${createLink(controller: "query", action: "ajaxRetrieveSampleCountForSpecies", id: BBSpeciesInstance.id)}');
});
                </g:javascript>

            </tr>

            </tbody>
        </table>
    </div>

    <div class="clr"></div>
</div>

<!-- only show if no organ is selected -->
<g:if test="${BBOrganInstance == null}">
    <div class="article">
        <div class="left">
            <h2 class="star">Related Organs</h2>


            <g:render template="../BBOrganAndSpeciesQuery/relatedOrgans" plugin="binbase-web-gui"  model="[BBSpeciesInstance:BBSpeciesInstance,BBOrganInstance:BBOrganInstance]"/>

        </div>

        <div class="right">

            <h2 class="star">Sample Distribution by Organ</h2>

            <binbase:pieSampleByOrganForSpeciesDistribtution speciesId="${BBSpeciesInstance.id}"/>

        </div>

        <div class="clr"></div>
    </div>
</g:if>

<div class="article">
    <h2 class="star">Related Experiments</h2>
    <g:render template="../BBOrganAndSpeciesQuery/relatedExperiments" plugin="binbase-web-gui"  model="[BBSpeciesInstance:BBSpeciesInstance,BBOrganInstance:BBOrganInstance]"/>

    <div class="clr"></div>

</div>

<div class="article">
    <h2 class="star">Related Bins</h2>

    <g:render template="../BBOrganAndSpeciesQuery/relatedBins" plugin="binbase-web-gui"  model="[BBSpeciesInstance:BBSpeciesInstance,BBOrganInstance:BBOrganInstance]"/>


    <div class="clr"></div>

</div>


</body>
</html>
