<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>

</head>

<body>
<div class="article">

    <h2>Search Results</h2>

    <g:if test="${flash.message}">
        <div class="message">${flash.message}</div>
    </g:if>
    <div class="clr"></div>
</div>


<g:if test="${results != null}">

<g:if test="${results.size() == 0}">
    <div class="article">

        <div class="message">
            sorry we didn't find any results for your query!
        </div>
    </div>
</g:if>
<g:else>
<g:if test="${results.bin != null}">

    <div class="article">
        <h2>Bins</h2>

        <div class="list">
            <table>
                <thead>
                <tr>

                    <th>Bin Id</th>
                    <th>Name</th>

                </tr>
                </thead>
                <tbody>
                <g:each in="${results.bin}" status="i" var="BBBinInstance">
                    <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">

                        <td><g:link controller="BBBin" action="show"
                                    id="${BBBinInstance.id}"><g:formatNumber number="${BBBinInstance.binbaseBinId}"
                                                                             format="########0"/></g:link></td>

                        <td>${fieldValue(bean: BBBinInstance, field: "name")}</td>

                    </tr>
                </g:each>
                </tbody>
            </table>
        </div>

        <div class="clr"></div>
    </div>

</g:if>
<g:if test="${results.spectra != null}">

    <div class="article">
        <h2>Spectra</h2>

        <div class="list">
            <table>
                <thead>
                <tr>

                    <th>Id</th>
                    <th>Name</th>
                    <th>Retention Index</th>

                </tr>
                </thead>
                <tbody>
                <g:each in="${results.spectra}" status="i" var="spectra">
                    <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">

                        <td><g:link controller="BBSpectra" action="show"
                                    id="${spectra.id}"><g:formatNumber
                                    number="${spectra.id}"
                                    format="########0"/></g:link></td>

                        <td>${spectra.bin.name}</td>

                        <td>${spectra.retentionIndex}</td>

                    </tr>
                </g:each>
                </tbody>
            </table>
        </div>


        <div class="clr"></div>
    </div>

</g:if>
<g:if test="${results.sample != null}">

    <div class="article">
        <h2>Samples</h2>

        <div class="list">
            <table>
                <thead>
                <tr>

                    <th>Id</th>
                    <th>File Name</th>

                </tr>
                </thead>
                <tbody>
                <g:each in="${results.sample}" status="i" var="sample">
                    <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">

                        <td><g:link controller="BBExperimentSample" action="show"
                                    id="${sample.id}"><g:formatNumber
                                    number="${sample.id}"
                                    format="########0"/></g:link></td>

                        <td>${sample.fileName}</td>

                    </tr>
                </g:each>
                </tbody>
            </table>
        </div>

        <div class="clr"></div>
    </div>

</g:if>
<g:if test="${results.clazz != null}">

    <div class="article">
        <h2>Classes</h2>

        <div class="list">
            <table>
                <thead>
                <tr>

                    <th>Name</th>

                </tr>
                </thead>
                <tbody>
                <g:each in="${results.clazz}" status="i" var="clazz">
                    <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">

                        <td><g:link controller="BBExperimentClass" action="show"
                                    id="${clazz.id}">${clazz.name}</g:link></td>

                    </tr>
                </g:each>
                </tbody>
            </table>
        </div>

        <div class="clr"></div>
    </div>

</g:if>
<g:if test="${results.experiment != null}">

    <div class="article">
        <h2>Experiments</h2>

        <div class="list">
            <table>
                <thead>
                <tr>

                    <th>Name</th>
                    <th>Title</th>

                </tr>
                </thead>
                <tbody>
                <g:each in="${results.experiment}" status="i" var="exp">
                    <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">

                        <td><g:link controller="BBExperiment" action="show"
                                    id="${exp.id}">${exp.name}</g:link></td>
                        <td>${exp.title }</td>

                    </tr>
                </g:each>
                </tbody>
            </table>
        </div>

        <div class="clr"></div>
    </div>

</g:if>
<g:if test="${results.species != null}">

    <div class="article">
        <h2>Species</h2>

        <div class="list">
            <table>
                <thead>
                <tr>

                    <th>Name</th>

                </tr>
                </thead>
                <tbody>
                <g:each in="${results.species}" status="i" var="exp">
                    <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">

                        <td><g:link controller="BBSpecies" action="show"
                                    id="${exp.id}">${exp.name}</g:link></td>

                    </tr>
                </g:each>
                </tbody>
            </table>
        </div>

        <div class="clr"></div>
    </div>

</g:if>

<g:if test="${results.organ != null}">

    <div class="article">
        <h2>Organs</h2>

        <div class="list">
            <table>
                <thead>
                <tr>

                    <th>Name</th>

                </tr>
                </thead>
                <tbody>
                <g:each in="${results.organ}" status="i" var="exp">
                    <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">

                        <td><g:link controller="BBOrgan" action="show"
                                    id="${exp.id}">${exp.name}</g:link></td>

                    </tr>
                </g:each>
                </tbody>
            </table>
        </div>

        <div class="clr"></div>
    </div>

</g:if>

<g:if test="${results.other != null}">

    <div class="article">
        <h2>Other</h2>

        <div class="clr"></div>
    </div>

</g:if>
</g:else>
</g:if>

</body>
</html>
