<table>
    <thead>
    <tr>

        <th class="ui-state-default" >
            comment
        </th>
        <th class="ui-state-default" >
            label
        </th>

        <th class="ui-state-default" >
            file name
        </th>

    </tr>
    </thead>
    <tbody>

    <g:each in="${c.samples}" var="sample">
        <tr>

            <td>${sample.comment}</td>
            <td>${sample.label}</td>
            <td><g:link controller="BBExperimentSample" action="show"
                    id="${sample.id}">${sample.fileName}</g:link></td>

        </tr>
    </g:each>
    </tbody>
</table>
