<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>

</head>

<body>

<div class="article">

    <g:javascript>
        jQuery(document).ready(function() {
            $('#classArcodion').accordion();
            $('#classArcodion').bind('accordionchange', function(event, ui) {

                //load the content now to increase the performance
            });

        });

    </g:javascript>

    <h2 class="star">Class Details: ${BBExperimentClassInstance.name}</h2>

    <g:render template="details" model="[c:BBExperimentClassInstance]"/>

    <div id="classArcodion">

        <h2><a href="#">Samples</a></h2>

        <div class="height320px">

            <g:render template="samples" model="[c:BBExperimentClassInstance]"/>
        </div>

        <h2><a href="#">MetaData</a></h2>

        <div class="height320px">
            <g:render template="metadata" model="[c:BBExperimentClassInstance]"/>

        </div>
    </div>
</div>

</body>
</html>
