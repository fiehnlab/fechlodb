<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>

</head>

<body>

<div class="article">

    <h2 class="star">Spectra Details</h2>
    <g:if test="${flash.message}">
        <div class="message">${flash.message}</div>
    </g:if>
    <div class="dialog">
        <table>
            <tbody>


            <tr class="prop">
                <td></td>

                <td></td>

                <td class="value" rowspan="8">
                    <binbase:renderSpectra id="${BBSpectraInstance.id}"/>
                </td>
            </tr>
            <tr class="prop">
                <td valign="top" class="name"><g:message code="BBSpectra.basePeak.label" default="Base Peak"/></td>

                <td valign="top" class="value">${fieldValue(bean: BBSpectraInstance, field: "basePeak")}</td>

            </tr>


            <tr class="prop">
                <td valign="top" class="name"><g:message code="BBSpectra.intensity.label" default="Intensity"/></td>

                <td valign="top" class="value"><g:formatNumber format="########0"
                                                               number="${BBSpectraInstance.intensity}"/></td>

            </tr>

            <tr class="prop">
                <td valign="top" class="name"><g:message code="BBSpectra.retentionIndex.label"
                                                         default="Retention Index"/></td>

                <td valign="top" class="value"><g:formatNumber format="########0"
                                                               number="${BBSpectraInstance.retentionIndex}"/></td>

            </tr>

            <tr class="prop">
                <td valign="top" class="name"><g:message code="BBSpectra.retentionTime.label"
                                                         default="Retention Time"/></td>

                <td valign="top" class="value"><g:formatNumber format="########0.00"
                                                               number="${BBSpectraInstance.retentionTime}"/>s</td>

            </tr>


            <tr class="prop">
                <td valign="top" class="name"><g:message code="BBSpectra.similarity.label" default="Similarity"/></td>

                <td valign="top" class="value"><g:formatNumber format="########0"
                                                               number="${BBSpectraInstance.similarity}"/></td>

            </tr>

            <tr class="prop">
                <td valign="top" class="name"><g:message code="BBSpectra.sn.label" default="Sn"/></td>

                <td valign="top" class="value"><g:formatNumber format="########0"
                                                               number="${BBSpectraInstance.sn}"/></td>

            </tr>

            <tr class="prop">
                <td valign="top" class="name"><g:message code="BBSpectra.uniqueMass.label" default="Unique Mass"/></td>

                <td valign="top" class="value">${fieldValue(bean: BBSpectraInstance, field: "uniqueMass")}</td>
            </tr>

            <tr class="prop">
                <td valign="top" class="name"><g:message code="BBSpectra.apexMasses.label" default="Apex Masses"/></td>

                <td valign="top" class="value" colspan="2">${fieldValue(bean: BBSpectraInstance, field: "apexMasses")}</td>

            </tr>

            <tr class="prop">
                <td valign="top" class="name"><g:message code="BBSpectra.bin.label" default="Bin"/></td>

                <td valign="top" class="value" colspan="2"><g:link controller="BBBin" action="show"
                                                       id="${BBSpectraInstance?.bin?.id}">${BBSpectraInstance?.bin?.name}</g:link></td>

            </tr>


            <tr class="prop">
                <td valign="top" class="name"><g:message code="BBSpectra.sample.label" default="Sample"/></td>

                <td valign="top" class="value" colspan="2"><g:link controller="BBExperimentSample" action="show"
                                                       id="${BBSpectraInstance?.sample?.id}">${BBSpectraInstance?.sample?.fileName}</g:link></td>

            </tr>
            </tbody>
        </table>
    </div>

</div>
</body>
</html>
