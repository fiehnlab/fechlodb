<!-- renders the bins for this species and organ selection -->
<g:setProvider library="jquery"/>


<g:javascript>
             $(function() {


        $(document).ready(function() {

            $('#organ_for_species').dataTable({
            bProcessing: true,
          bServerSide: true,
          sAjaxSource: '${createLink(controller: "BBOrganAndSpeciesQuery", action: "ajaxOrganBySpeciesAsJSON", params: [speciesId: BBSpeciesInstance?.id], plugin: "binbase-web-gui")}' ,
          bJQueryUI: true,
"bAutoWidth" : false,
"bPaginate": false,
		"bInfo": false,
		"bLengthChange": false,
	 "aoColumns" : [
	 	{ sWidth : "20%" },
	 	{ sWidth : "80%" }
 			 ],


          "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
		"bFilter": false
            });
        });
    });
</g:javascript>
<table id="organ_for_species" class="hover_table">
    <thead>
    <tr>
        <th>id</th>
        <th>organ name</th>
    </tr>
    </thead>

    <!-- will contain the actual content -->
    <tbody>
    </tbody>
</table>

<!-- used to load the related bin on the click on a row -->
<g:javascript>
    $(function() {

        $(document).ready(function() {

            $('#organ_for_species tbody tr').live('click', function() {
                var nTds = $('td', this);
                var textId = $(nTds[0]).text();

                window.location = '${createLink(controller: "BBOrgan", action: "show")}'+'/'+textId + "?speciesId=${BBSpeciesInstance?.id}";
            });


        });
    });
</g:javascript>
