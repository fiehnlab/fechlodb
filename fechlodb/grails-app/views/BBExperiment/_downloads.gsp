<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>

</head>

<body>
<div class="article">
    <h2 class="star">Results for ${exp.name}</h2>

    <g:if test="${flash.message}">
        <div class="message">${flash.message}</div>
    </g:if>

    <binbase:tableResultsForExperiment experimentId="${exp.id}"/>

</div>
</body>
</html>
