<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>

</head>

<body>
<div class="article">

    <h2 class="star">Experiment Browser</h2>

    <g:if test="${flash.message}">
        <div class="message">${flash.message}</div>
    </g:if>
    <div class="list">
        <table id="experiments" class="hover_table">
            <thead>
            <tr>
                <th>id</th>
                <th>name</th>
                <th>title</th>

            </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>

</div>


<g:javascript>
             $(function() {


        $(document).ready(function() {

            $('#experiments').dataTable({
            bProcessing: true,
          bServerSide: true,
          sAjaxSource: '${createLink(controller: "BBExperimentQuery", action: "ajaxListExperimentsAsJSON", plugin: "binbase-web-gui")}' ,
          bJQueryUI: true,
"bAutoWidth" : false,
	 "aoColumns" : [
	 	{ sWidth : "10%" },
	 	{ sWidth : "30%" },
	 	{ sWidth : "60%" }
 			 ],


          "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
		"bFilter": false
            });
        });
    });
</g:javascript>

<!-- used to load the related bin on the click on a row -->
<g:javascript>
    $(function() {

        $(document).ready(function() {

            $('#experiments tbody tr').live('click', function() {
                var nTds = $('td', this);
                var textId = $(nTds[0]).text();

                window.location = '${createLink(controller: "BBExperiment", action: "show")}'+'/'+textId;
            });


        });
    });
</g:javascript>

</body>
</html>
