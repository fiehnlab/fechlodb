<g:setProvider library="jquery"/>
<g:javascript>
        jQuery(document).ready(function() {

            $("#species_organ_tree_for_${bin.id}").treeTable({
                indent:20
            });


        });
</g:javascript>

<table id="species_organ_tree_for_${bin.id}">
    <thead>
    <tr>
        <th class="ui-state-default">species</th>
        <th class="ui-state-default">found in samples (abs)</th>
        <th class="ui-state-default">found in samples (rel)</th>
        <th class="ui-state-default">total sample count</th>
    </tr>
    </thead>
    <tbody>
    <g:set var="nodeId" value="${0}"/>
    <g:each in="${data}" var="map" status="i">
        <tr id="node-${nodeId}">
            <td style="padding-left: 18px;"><g:link controller="BBSpecies" action="show"
                                                    id="${map.species_id}">${map.species}</g:link></td>
            <td>${map.foundInSamples}</td>
            <td><g:formatNumber number="${map.foundInSamples/map.totalSamples*100}" maxFractionDigits="2"/>%</td>

            <td>${map.totalSamples}</td>
        </tr>


        <g:set var="parentNodeId" value="${nodeId}"/>

        <g:set var="nodeId" value="${nodeId+1}"/>

        <tr id="node-${nodeId}" class="child-of-node-${parentNodeId}">
            <td colspan="4" style="padding-right: 38px">

                <div id="${nodeId}_${parentNodeId}">
                    <img src="${resource(dir: 'images', file: 'spinner.gif')}"
                         alt="${message(code: 'spinner.alt', default: 'Loading...')}"/>
                </div>
            </td>

        </tr>

        <g:javascript>
            jQuery(document).ready(function() {
                $("#${nodeId}_${parentNodeId}").load('${createLink(controller: "BBBinQuery", action: "ajaxOrgansForSpeciesAndBin", plugin: 'binbase-web-gui', id: bin.id, params: [species: map.species_id, parent: parentNodeId])}');
            });
        </g:javascript>

        <g:set var="nodeId" value="${nodeId+1}"/>

    </g:each>
    </tbody>
</table>