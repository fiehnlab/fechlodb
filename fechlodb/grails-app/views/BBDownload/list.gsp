<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>

</head>

<body>
<div class="article">

    <h2 class="star">Downloads</h2>

    <g:if test="${flash.message}">
        <div class="message">${flash.message}</div>
    </g:if>
    <div class="list">
        <table>
            <thead>
            <tr>

                <th>Filename</th>
                <th>Description</th>

            </tr>
            </thead>
            <tbody>
            <g:each in="${result}" status="i" var="res">
                <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">

                    <td><g:link action="download"
                            id="${res.id}">${res.fileName}</g:link></td>

                    <td>${res.description}</td>

                </tr>
            </g:each>
            </tbody>
        </table>
    </div>

</div>
</body>
</html>
